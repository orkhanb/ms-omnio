package az.atl.msauth.model;

import az.atl.msauth.dao.entity.UserEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterResponse {
    String firstName;
    String lastName;
    String userName;
    String email;
    String jobTitle;
    String password;
    Role role;

    public static RegisterResponse buildRegisterDto(UserEntity userEntity) {
        return RegisterResponse.builder()
                .firstName(userEntity.getFirstname())
                .lastName(userEntity.getLastname())
                .userName(userEntity.getUsername())
                .email(userEntity.getEmail())
                .jobTitle(userEntity.getJobTitle())
                .password(userEntity.getPassword())
                .role(userEntity.getRole())
                .build();
    }
}
