package az.atl.msauth.model.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ExceptionMessages {
    USER_NOT_FOUND("userNotFound"),
    INVALID_OLD_PASSWORD("invalidOldPassword"),
    USERNAME_ALREADY_EXIST("usernameAlreadyExist");
    private final String message;
}
