package az.atl.msauth.model.dto;

import az.atl.msauth.model.Role;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserProfileDto {
    Long id;
    String name;
    String surname;
    String userName;
    String email;
    String jobTitle;
    @Enumerated(EnumType.STRING)
    Role role;
}
