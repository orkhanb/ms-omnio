package az.atl.msauth.client;

import az.atl.msauth.model.SendMessageRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "messageClient", url = "http://localhost:8889/")
public interface MessageClient {

    @RequestMapping(method = RequestMethod.POST, value = "/send")
    void sendMessage(@RequestBody SendMessageRequest messageRequest);
}
