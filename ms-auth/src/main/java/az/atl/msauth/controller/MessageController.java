package az.atl.msauth.controller;

import az.atl.msauth.model.SendMessageRequest;
import az.atl.msauth.service.MessageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/messages")
@Slf4j
public class MessageController {
    private final MessageService messageService;

    @PostMapping("/send")
    void sendMessage(@RequestBody SendMessageRequest messageRequest) {
        log.info("message sent");
        messageService.sendMessage(messageRequest);
    }
}

