package az.atl.msauth.controller;

import az.atl.msauth.model.consts.UpdatePasswordRequest;
import az.atl.msauth.model.dto.UserProfileDto;
import az.atl.msauth.service.UserProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/profile")
@RequiredArgsConstructor
public class UserProfileController {
    private final UserProfileService userProfileService;

    @GetMapping("/{id}")
    public UserProfileDto getUserProfileById(@PathVariable Long id) {
        return userProfileService.getUserProfileById(id);
    }

    @PutMapping("/{id}/password")
    public ResponseEntity<String> updatePassword(
            @PathVariable("id") Long userId,
            @RequestBody UpdatePasswordRequest updatePasswordRequest
    ) {
        String oldPassword = updatePasswordRequest.getOldPassword();
        String newPassword = updatePasswordRequest.getNewPassword();

        userProfileService.updatePassword(userId, oldPassword, newPassword);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @GetMapping("/getAllUsers")
    @ResponseStatus(HttpStatus.OK)
    public List<UserProfileDto> getAllUsers() {
        return userProfileService.getAllUsers();
    }

    @GetMapping("/getPaginatedUsers")
    public Page<UserProfileDto> getPaginatedUsers(Pageable pageable) {
        return userProfileService.getPaginatedUsers(pageable);
    }

    @PutMapping("/update-user/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public UserProfileDto updateUser(@PathVariable Long id, @RequestBody UserProfileDto userProfileDto) {
        return userProfileService.updateUser(id, userProfileDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    void deleteUserById(@PathVariable Long id) {
        userProfileService.deleteUserById(id);
    }
}
