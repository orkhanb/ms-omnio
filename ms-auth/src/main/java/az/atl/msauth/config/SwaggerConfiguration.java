package az.atl.msauth.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
//@SecurityScheme(
//        name = "Authorization",
//        scheme = "Bearer",
//        bearerFormat = "JWT",
//        type = SecuritySchemeType.HTTP,
//        in = SecuritySchemeIn.HEADER
//)
public class SwaggerConfiguration {

    @Bean
    public OpenAPI omnioOpenApi() {
        return new OpenAPI()
                .info(new Info()
                        .title("Omnio Api")
                        .description("Omnio application")
                        .version("1.0")
                        .license(new License()));


//            Contact contact = new Contact();
//            contact.setEmail("bezkoder@gmail.com");
//            contact.setName("BezKoder");
//            contact.setUrl("https://www.bezkoder.com");
//
//            Info info = new Info()
//                    .title("Demo Omnio App")
//                    .version("1.0")
//                    .contact(contact)
//                    .description("This API exposes endpoints to manage tutorials.")
//                    .termsOfService("https://www.bezkoder.com/terms");
//        return new OpenAPI().info(info)
//                .addSecurityItem(
//                new SecurityRequirement().addList("Authorization"));
    }
}
