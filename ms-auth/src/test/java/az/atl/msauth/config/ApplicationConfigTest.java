package az.atl.msauth.config;

import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ApplicationConfigTest {
    @Test
    public void testPasswordEncoder() {
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        String rawPassword = "myPassword";

        String encodedPassword = passwordEncoder.encode(rawPassword);

        assertTrue(encodedPassword != null && !encodedPassword.isEmpty());

        assertTrue(passwordEncoder.matches(rawPassword, encodedPassword));
    }

}
