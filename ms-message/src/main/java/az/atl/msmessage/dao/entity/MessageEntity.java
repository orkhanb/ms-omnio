package az.atl.msmessage.dao.entity;

import az.atl.msmessage.model.consts.Channels;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table(name = "messages")

public class MessageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    UserEntity sender;

    @ManyToOne
    @JoinColumn(name = "recipient_id")
    UserEntity recipient;

    String content;

    LocalDateTime createdAt;
    @Enumerated(EnumType.STRING)
    Channels channels;
}
