package az.atl.msmessage.model;

import az.atl.msmessage.model.consts.Channels;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MessageDto {
    Long recipientUserId;
    String content;
    Channels channels;
}
